package rbattleshipClient;

public class BattleshipObjects {
	
	private String name;
	private String[] position = new String[3];
	// Used for understanding which ships have been destroyed
	private boolean[] destroyed = {false, false, false};
	static public Character[] alphabet = new Character[7];
	
	static {
		int i = 0;
		for (char alpha = 'A'; alpha <= 'G'; alpha++){
			alphabet[i] = alpha;
			i++;
		}
	} // close static
	
	public BattleshipObjects(String nm) {
		name = nm;
		// creates a battleship parallel to the y-axis
		if (Math.random() < 0.5){
			createVertical();
		}
		// creates a battleship parallel to the x-axis
		else{
			createHorizontal();
		}
	} // close constructor
	
	private void createVertical() {
		int numX = (int) (Math.random() * 7);
		int numY = (int) (Math.random() * 5) + 1;
		for (int i = 0; i < 3; i++){
			position[i] = alphabet[numX].toString() + (numY + i);
		}
	} // close createVertical
	
	private void createHorizontal() {
		int numX = (int) (Math.random() * 5);
		int numY = (int) (Math.random() * 7) + 1;
		for (int i = 0; i < 3; i++){
			position[i] = alphabet[numX + i].toString() + numY;
		}
	} // close createHorizontal
	
	public String getPosition(int index) {
		return position[index];
	} // close getPosition
	
	public boolean getDestroyed(int index) {
		return destroyed[index];
	} // close getDestroyed
	
	public String getName() {
		return name;
	} // close getName
	
	public void setDestroyed(int index) {
		destroyed[index] = true;
	} // close setDestroyed

} // close BattleshipObjects