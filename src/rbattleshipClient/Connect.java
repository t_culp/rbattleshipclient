package rbattleshipClient;

import java.net.*;
import java.io.*;

public class Connect {

	private static Connect connect;
	private String selectedPosition = null; // contains this client's guess
	private ObjectOutputStream output = null; // used to send data
	private ObjectInputStream input = null; // used to receive data
	private UserFrontEnd gui; // GUI instance
	private boolean startGame = false;
	private boolean endGame = false;
	private int battleshipHit = 0; // 0 means data not received, 1 means hit, anything else means not hit (probably 2)
	private int battleshipDestroyed = 0; // see line above
	private String battleshipPosition = null; // contains the opponent's guess
	private Socket socket;
	private Thread readGuessThread = null;
	private Thread sendGuessThread = null;
	private boolean disconnecting = false;
	private String username = null;

	public static void main(String[] args) {
		Connect connect = new Connect();
		connect.start();
	} // close main

	private void start() {
		try {
			connect = this;
			gui = new UserFrontEnd(this);
			gui.createGameGUI();
			String IPAddress = gui.getServerIP();
			int port = gui.getServerPort();
			socket = new Socket(IPAddress == "" ? "localhost" : IPAddress, port);
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
			username = login();
			if (username == null)
				return;
			gui.setTitle(username);
			gui.setConnected(true);
			Thread serverMonitor = new Thread(new ServerMonitor());
			serverMonitor.setName("Server monitor (from start())");
			serverMonitor.start(); // the server monitor will watch the inputs from the server and respond appropriately
			gameplay();
		} catch (NullPointerException ex) {
			gui.updateTextArea("You did not connect to the server. You may connect with the Connect button.");
		} catch (Exception ex) {
			gui.updateTextArea("\n\n\n*\n*\n*\nYOU OR THE SERVER HAS ENCOUNTERED AN ERROR. PLEASE RECONNECT LATER.\n*\n*\n*");
			String cause = ex.getLocalizedMessage();
			if (cause != null)
				gui.updateTextArea("\n" + cause);
			if (!disconnecting)
				disconnect();
		}
	} // close start
	
	// this method does all that start() does except create a gui
	class Reconnect implements Runnable {
		public void run() {
			disconnecting = false;
			try {
				String IPAddress = gui.getServerIP();
				int port = gui.getServerPort();
				socket = new Socket(IPAddress == "" ? "localhost" : IPAddress, port);
				output = new ObjectOutputStream(socket.getOutputStream());
				input = new ObjectInputStream(socket.getInputStream());
				String username = login();
				if (username == null)
					return;
				gui.setTitle(username);
				gui.setConnected(true);
				Thread serverMonitor = new Thread(new ServerMonitor());
				serverMonitor.setName("Server monitor (from reconnect())");
				serverMonitor.start(); // the server monitor will watch the inputs from the server and respond appropriately
				gameplay();
			} catch (Exception ex) {
				gui.updateTextArea("\n\n\n*\n*\n*\nYOU OR THE SERVER HAS ENCOUNTERED AN ERROR. PLEASE RECONNECT LATER.\n*\n*\n*");
				String cause = ex.getLocalizedMessage();
				if (cause != null) 
					gui.updateTextArea("\n" + cause);
				if (!disconnecting)
					disconnect();
			}
		} // close run
	
	} // close Reconnect
		
		
	// close the socket, exit everything gracefully, etc.
	public void disconnect() {
		if (socket == null) {
			return;
		}
		gui.setConnected(false);
		disconnecting = true;
		if (!socket.isClosed()) {
			try {
				socket.close();
			} catch (IOException e) {}
		}
		endGame = true;
		waitWhileThreadsDie();
		endGame = false;
		clearOldData();
	} // close disconnect		
	
	private void gameplay() throws IOException {
		// begin actual gameplay
		try {
			while (!startGame) { // waits until connected to another client
				Thread.sleep(500);
			}
		} catch (InterruptedException e) {}
		startGame = false; // for when the game restarts
		gui.enableChat(true);
		try {
			while (!endGame) {
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {}
		waitWhileThreadsDie();
		endGame = false; // for when the next game ends
		gui.enableChat(false);
		gui.resetFields();
		gameplay(); // this recursive call keeps the game running as long as the client is connected.
	} // close gameplay
	
	// gives username information to the server selected
	private String login() throws IOException {
		String username = null;
		boolean allowed = input.readBoolean();
		if (allowed) {
			boolean validUsername = false;
			while (!validUsername) {
				username = gui.promptUsername();
				boolean nullUsername = false;
				if (username == null) {
					nullUsername = true;
					gui.showNullUsername();
				} else {
					output.writeObject(username);
					output.flush();
					validUsername = input.readBoolean();
				}
				if (!(validUsername || nullUsername)) {
					String usernames = null;
					try {
						usernames = (String) input.readObject();
					} catch (ClassNotFoundException e) {}
					gui.showBadUsername(usernames);
				}
			}
		}
		else {
			gui.updateTextArea("\n\n\n[SERVER]: This IP has been banned from this server. You will not be able to connect to this server.");
		}
		return username;
	} // close login
	
	// this method is used by the GUI to set the user's choice
	public void setSelectedPosition(String position) {
		selectedPosition = position;
	} // close setSelectedPosition
	
	// easy way to send a message to the opponent (keeps code free of encoding lines)
	public static void sendMessage(String message) {
		if (!message.equals("")) {
			if (message.length() > 5 && message.substring(0, 6).equals("/msg ")) {
				String username = message.substring(5);
				if (!username.contains(" ")) {
					connect.gui.updateTextArea("[SERVER]: Invalid username or message. Your message was not sent.");
					return;
				}
			} else if (message.length() > 1 && !message.substring(0, 1).equals("/")) {
				connect.gui.updateTextArea("[" + connect.username + "]: " + message);
			}
			try {
				connect.output.writeInt(1); // indicate that a chat string is following
				connect.output.writeObject(message);
				connect.output.flush();
			} catch (IOException e) {}
		} // close if(!message.equals(""))
	} // close sendMessage
	
	private void waitWhileThreadsDie() {
		while (((readGuessThread != null) ? readGuessThread.isAlive() : false) || ((sendGuessThread != null) ? sendGuessThread.isAlive() : false)) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
	} // close waitWhileThreadsDie
	
	private void clearOldData() {
		selectedPosition = null;
		output = null;
		input = null;
		startGame = false;
		endGame = false;
		battleshipHit = 0; // 0 if not hit, 1 if hit
		battleshipDestroyed = 0; // 0 if not destroyed, 1 if destroyed
		battleshipPosition = null;
		readGuessThread = null;
		sendGuessThread = null;
	} // close clearOldData
	
	/*
	 * DATA ENCODING:
	 * 0: heartbeat
	 * 2: here comes a chat string
	 * 3: here comes a battleship position
	 * 4: game has ended, more instructions following.
	 * 5: there has been a connection error, you are getting sent back to the lobby
	 * 6: start game
	 * 7: send battleship position
	 * 8: read whether a ship was hit or not
	 * 9: ship was destroyed
	 * 10: position string
	 */
	
	class ServerMonitor implements Runnable {
		
		public void run() {
			try {
				while (true) {
					while (startGame) { // this will keep the program from killing the SendGuess thread on restart
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {}
					}
					int code = input.readInt();
					if (code == 0) {
						// heartbeat
					} else if (code == 2) {
						String message = (String) input.readObject();
						gui.updateTextArea(message);
						// chat string
					} else if (code == 3) {
						readGuessThread = new Thread(new ReadGuess());
						readGuessThread.setName("Read guess thread");
						readGuessThread.start();
						// wait for the battleship stuff
					} else if (code == 4) {
						output.writeInt(2);
						output.flush();
						try {
							Thread.sleep(1000);
							/*
							 * I'm going to leave out this code because I think it is causing some problems in perhaps
							 * other versions of Java and maybe other computers. This piece of code really isn't even
							 * that important outside of aesthetics anyway and the Thread.sleep(1000) ought to take 
							 * care of any of those issues.
							while (Thread.activeCount() > 3) {
								Thread.sleep(100); //wait for other threads to die before printing messages
							}
							*/
						} catch (InterruptedException e) {}
						if (input.readBoolean())
							gui.updateTextArea("Congratulations! You won!!!");
						else 
							gui.updateTextArea("Sorry, you lost.");
						endGame = true;
						// game has ended
					} else if (code == 5) {
						endGame = true;
						output.writeInt(2);
						output.flush();
						// connection error
					} else if (code == 6) {
						startGame = true;
						// close start game
					} else if (code == 7) {
						sendGuessThread = new Thread(new SendGuess());
						sendGuessThread.setName("Send guess thread");
						sendGuessThread.start();
						// close send battleship position
					} else if (code == 8) {
						battleshipHit = input.readInt();
						// battleship was hit or not
					} else if (code == 9) {
						battleshipDestroyed = input.readInt();
						// battleship was destroyed or not
					} else if (code == 10) {
						battleshipPosition = (String) input.readObject();
						// actual position string
					} else if (code == 11) {
						throw new ServerOfflineException();
						// close server shut down
					} else if (code == 12) {
						throw new BannedException();
						// close banned
					} 
				}
			} catch (BannedException e) {
				if (!disconnecting) {
					gui.updateTextArea("\n\n[SERVER]: You have been banned from this server.");
					disconnect();
				}
			} catch (ServerOfflineException e) {
				if (!disconnecting) {
					gui.updateTextArea("\n\n\n[SERVER]: The server has disconnected. Please reconnect later.");
					disconnect();
				}
			} catch (IOException e) {
				if (!disconnecting) {
					gui.updateTextArea("\n\n\n*\n*\n*\nYOU OR THE SERVER HAS ENCOUNTERED AN ERROR. PLEASE RECONNECT LATER.\n*\n*\n*");
					disconnect();
				}
			} 
			  catch (ClassNotFoundException e) {}
		} // close run
		
	} // close MonitorChat
	
	/*
	 * Thread dedicated to receiving messages so that chat and other communications may proceed
	 */
	class ReadGuess implements Runnable {
		
		public void run() {
			try {
				while (battleshipHit == 0 && !endGame) {
					Thread.sleep(250);
				}
				if (endGame)
					return;
				boolean hit = (battleshipHit == 1) ? true : false;
				battleshipHit = 0;
				while (battleshipPosition == null && !endGame) {
					Thread.sleep(250);
				}
				if (endGame)
					return;
				String position = battleshipPosition;
				battleshipPosition = null;
				if (hit) {
					gui.updateTextArea("Opponent HITS: " + position);
					while (battleshipDestroyed == 0 && !endGame) {
						Thread.sleep(250);
					}
					if (endGame) {
						return;
					}
					boolean shipDestroyed = battleshipDestroyed == 1 ? true : false;
					battleshipDestroyed = 0;
					if (shipDestroyed) {
						gui.updateTextArea("One of your ships has been destroyed!");
					}
				} else {
					gui.updateTextArea("Opponent selects: " + position);
				}
				gui.updateUserField(position, hit);
			} catch (InterruptedException e) {}
		} // close run
		
	} // close ReadGuess
	
	/*
	 * Thread dedicated to sending a guess so that chat and other communications may proceed
	 */
	class SendGuess implements Runnable {
		
		public void run() {
			gui.setPlayable();
			try {
				while (selectedPosition == null && !endGame) {
					Thread.sleep(250);
				}
				if (endGame)
					return;
				output.writeInt(0); // indicate a guess is being sent, not a message
				output.writeObject(selectedPosition);
				output.flush();
				while (battleshipHit == 0) {
					Thread.sleep(250);
				}
				if (endGame) 
					return;
				boolean hit = battleshipHit == 1 ? true : false;
				battleshipHit = 0;
				if (hit) {
					gui.markEnemyFieldAsHit(selectedPosition);
					while (battleshipDestroyed == 0 && !endGame) {
						Thread.sleep(250);
					}
					if (endGame)
						return;
					boolean sunkBattleship = (battleshipDestroyed == 1 ? true : false);
					battleshipDestroyed = 0;
					if (sunkBattleship)
						gui.updateTextArea("You have sunk an opponent's battleship!");
				}
			} catch (IOException e) {
			} catch (InterruptedException e) {}
			selectedPosition = null;
		} // close run
		
	} // close SendGuess
	
	class ServerOfflineException extends Exception {
		
		private static final long serialVersionUID = 1L;
		
	} // close ServerOffline
	
	class BannedException extends Exception {
		
		private static final long serialVersionUID = 1L;
		
	} // close BannedException
	
} // close Connect