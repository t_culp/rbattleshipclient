package rbattleshipClient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

public class UserFrontEnd {
	
	private JButton[] enemyField = new JButton[49]; // 7x7 fields
	private boolean[] enemyClicked = new boolean[49];
	private JButton[] userField = new JButton[49];
	private boolean[] userClicked = new boolean[49];
	private JPanel enemyPanel; // lower panel
	private JPanel userPanel; // upper panel
	private JTextArea textArea;
	private JFrame frame;
	private boolean myTurn = false;
	private Connect user; 
	private JButton send;
	private JButton connect;
	private JTextField chatEntry;
	
	private final Color UNCLICKED_TURN = new Color(255, 90, 90);
	private final Color UNCLICKED_NOT_TURN = new Color(238, 238, 238);
	private final Color CLICKED = new Color(29, 150, 239);
	
	public UserFrontEnd(Connect user) {
		this.user = user;
	} // close constructor
	
	public static void main(String[] args) {
		/*
		 * This main is simply for testing purposes. It is not used during actual gameplay.
		 */
		UserFrontEnd gui = new UserFrontEnd(new Connect());
		gui.createGameGUI();
	} // close main
	
	public void createGameGUI() {
		// initialize boolean click arrays
		for (int i = 0; i < 49; i++) {
			enemyClicked[i] = false;
			userClicked[i] = false;
		}
		
		// button creation and dimensioning
		BattleshipButtonListener battleshipListener = new BattleshipButtonListener();
		Dimension standardDimension = new Dimension(40, 40);
		Font standardFont = new Font("Arial", Font.BOLD, 15);
		for (int i = 0; i < 49; i++) {
			enemyField[i] = new JButton();
			enemyField[i].setPreferredSize(standardDimension);
			enemyField[i].setMinimumSize(standardDimension);
			enemyField[i].setFont(standardFont);
			enemyField[i].setBackground(UNCLICKED_NOT_TURN);
			enemyField[i].addActionListener(battleshipListener);
			userField[i] = new JButton();
			userField[i].setPreferredSize(standardDimension);
			userField[i].setMinimumSize(standardDimension);
			userField[i].setFont(standardFont);
			userField[i].setEnabled(false);
			userField[i].setBackground(UNCLICKED_NOT_TURN);
		}
		
		// name tags
		JLabel userFieldLabel = new JLabel("YOU");
		userFieldLabel.setForeground(Color.blue);
		JLabel enemyFieldLabel = new JLabel("OPPONENT");
		enemyFieldLabel.setForeground(Color.red);
		
		// label creation
		JLabel[] userLetterLabels = new JLabel[7];
		JLabel[] userNumberLabels = new JLabel[7];
		JLabel[] enemyLetterLabels = new JLabel[7];
		JLabel[] enemyNumberLabels = new JLabel[7];
		for (int i = 0; i < 7; i++) {
			char tmp = (char) (i + 65);
			String letter = "" + tmp;
			userLetterLabels[i] = new JLabel(letter);
			enemyLetterLabels[i] = new JLabel(letter);
			String number = "" + (i + 1);
			userNumberLabels[i] = new JLabel(number);
			enemyNumberLabels[i] = new JLabel(number);
		}
		
		// frame setup
		frame = new JFrame("Battleship");
		frame.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		// add enemy name tag
		gc.gridx = 0;
		gc.gridy = 0;
		gc.weightx = 0.5;
		frame.getContentPane().add(enemyFieldLabel, gc);
		
		// computer panel setup
		enemyPanel = new JPanel();
		enemyPanel.setPreferredSize(new Dimension(300, 300));
		gc.gridx = 0;
		gc.gridy = 1; 
		gc.weightx = 0.5;
		frame.getContentPane().add(enemyPanel, gc);
		enemyPanel.setLayout(new GridBagLayout());
		
		// adding enemyField and labels to the computer panel
		gc.gridy = 0;
		for (int x = 1; x < 8; x++) {
			gc.gridx = x;
			enemyPanel.add(enemyLetterLabels[x-1], gc);
		}
		int pos = 0;
		for (int y = 1; y < 8; y++) {
		gc.gridy = y;
		gc.gridx = 0;
		enemyPanel.add(enemyNumberLabels[y - 1], gc);
			for (int x = 1; x < 8; x++) {
				gc.gridx = x;
				enemyPanel.add(enemyField[pos], gc);
				pos++;
			}
		}
		
		// add user name tag
		gc.gridx = 0;
		gc.gridy = 2;
		gc.weightx = 0.5;
		frame.getContentPane().add(userFieldLabel, gc);
		
		// user panel setup
		userPanel = new JPanel();
		userPanel.setPreferredSize(new Dimension(300, 300));
		userPanel.setLayout(new GridBagLayout());
		gc.gridy = 3; // sets userPanel underneath computerPanel
		gc.gridx = 0;
		gc.weightx = 0.5;
		frame.getContentPane().add(userPanel, gc);
		
		// adding userField and labels to the user panel
		gc.gridy = 0;
		for (int x = 1; x < 8; x++) {
			gc.gridx = x;
			userPanel.add(userLetterLabels[x - 1], gc);
		}
		pos = 0;
		for (int y = 1; y < 8; y++) {
			gc.gridy = y;
			gc.gridx = 0;
			userPanel.add(userNumberLabels[y - 1], gc);
			for (int x = 1; x < 8; x++) {
				gc.gridx = x;
				userPanel.add(userField[pos], gc);
				pos++;
			}
		}
		
		// adding text area to side
		JPanel textPanel = new JPanel();
		textPanel.setLayout(new GridBagLayout());
		gc.gridx = 1;
		gc.gridy = 0;
		gc.weightx = 0.5;
		gc.gridheight = 4;
		gc.fill = GridBagConstraints.BOTH;
		frame.getContentPane().add(textPanel, gc);
		textArea = new JTextArea(30, 30);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		DefaultCaret caret = (DefaultCaret) textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		JScrollPane scroller = new JScrollPane(textArea);
		scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		//scroller.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
		gc.gridx = 0;
		gc.weighty = 0.99;
		gc.gridheight = 1;
		gc.gridwidth = 3;
		textPanel.add(scroller, gc);
		textArea.setMaximumSize(textArea.getPreferredSize());
		textPanel.setMaximumSize(textPanel.getPreferredSize());
		
		// adding chatEntry
		chatEntry = new JTextField();
		chatEntry.setEnabled(false);
		chatEntry.setFocusable(true);
		chatEntry.addKeyListener(new EnterKeyListener());
		gc.gridx = 0;
		gc.gridy = 1;
		gc.weighty = 0.01;
		gc.gridheight = 1;
		gc.gridwidth = 1;
		gc.weightx = 0.9;
		textPanel.add(chatEntry, gc);
		
		// adding send button (reuses some old gc)
		send = new JButton("Send");
		send.addActionListener(new SendListener());
		send.setEnabled(false);
		gc.gridx = 1;
		gc.weightx = 0.1;
		textPanel.add(send, gc);
		
		// adding connect button
		connect = new JButton("Connect");
		connect.addActionListener(new ConnectListener());
		gc.gridx = 2;
		textPanel.add(connect, gc);

		// final frame stuff
		ImageIcon battleshipIcon = new ImageIcon(getClass().getResource("battleship.jpg"));
		frame.setSize(500, 500);
		frame.setIconImage(battleshipIcon.getImage());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.pack();
		frame.setVisible(true);
	} // close go
	
	public void updateUserField(String position, boolean hit) {
		// 'A' = 65
		// '1' = 49
		final boolean HIT = hit;
		position = position.toUpperCase();
		int letter = position.charAt(0);
		int number = position.charAt(1);
		final int positionInArray = (letter - 65) + ((number - 49) * 7);
		userClicked[positionInArray] = true;
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				if (HIT)
					userField[positionInArray].setText("<html><font color = red>!</font></html>");
			}
		});
	} // close updateComputerField
	
	public void markEnemyFieldAsHit(String position) {
		int letter = position.charAt(0);
		int number = position.charAt(1);
		final int positionInArray = (letter - 65) + ((number - 49) * 7);
		enemyClicked[positionInArray] = true;
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				enemyField[positionInArray].setText("<html><font color = red>!</font></html>");
			}
		});
	} // close markEnemyFieldAsHit
	
	public void setPlayable() {
		myTurn = true;
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				for (int i = 0; i < 49; i++) {
					enemyField[i].setBackground(enemyClicked[i] ? CLICKED : UNCLICKED_TURN);
					userField[i].setBackground(userClicked[i] ? CLICKED : UNCLICKED_NOT_TURN);
				}
			}
		});
		
	} // close setPlayable
	
	private void setUnplayable() {
		myTurn = false;
		for (int i = 0; i < 49; i++) {
			enemyField[i].setBackground(enemyClicked[i] ? CLICKED : UNCLICKED_NOT_TURN);
			userField[i].setBackground(userClicked[i] ? CLICKED : UNCLICKED_TURN);
		}
	} // close setUnplayable
	
	public void updateTextArea(String message) {
		final String MESSAGE = message;
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				textArea.append(MESSAGE + "\n");
			}
		});
	} // close updateTextArea 
	
	// converts integer indicating position in array to number-letter combination (ex. 'B3' or 'C5')
	class BattleshipButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (myTurn) {
				JButton btnClicked = (JButton) e.getSource();
				btnClicked.setEnabled(false);
				int i = 0;
				boolean buttonFound = false;
				while (!buttonFound) {
					if (btnClicked.equals(enemyField[i]))
						buttonFound = true;
					i++;
				}
				int positionInArray = i - 1;
				enemyClicked[positionInArray] = true;
				int number;
				if (i % 7 != 0)
					number = (i / 7) + 1;
				else
					number = i / 7;
				int intLetter = i + - ((number - 1) * 7);
				char letter = (char) ((intLetter - 1) + 65);
				String position = "" + letter + number;
				user.setSelectedPosition(position);
				setUnplayable();
			}
			
		} // close actionPerformed		
	} // close BattleshipButtons
	
	// send button listener
	class SendListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			String message = null;
			if (!(message = chatEntry.getText()).equals("")) {
				Connect.sendMessage(message);
				//updateTextArea("["+ frame.getTitle().substring(13) + "]: " + message);
				chatEntry.setText("");
				send.setEnabled(false);
			} else {
				send.setEnabled(false);
			}
		} // close actionPerformed
				
	} // close SendListener

	class EnterKeyListener implements KeyListener {
		/*
		 * (non-Javadoc)
		 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
		 * 
		 * This listens for any key while in messageField. It will disable
		 * the send button when there is no text in the box and enable it when
		 * there is. 
		 * It will send the message in messageField when the enter key has been 
		 * released
		 */

		public void keyPressed(KeyEvent arg0) {
			if (chatEntry.getText().equals(""))
				send.setEnabled(false);
			else
				send.setEnabled(true);
		} // close keyPressed

		public void keyReleased(KeyEvent arg0) {
			if (arg0.getKeyCode() == 10 && !chatEntry.getText().equals("")) {
				String message = chatEntry.getText();
				Connect.sendMessage(message);
				//updateTextArea("["+ frame.getTitle().substring(13) + "]: " + message);
				chatEntry.setText("");
				send.setEnabled(false);
			} else {
				if (chatEntry.getText().equals("")) {
					send.setEnabled(false);
				} else {
					send.setEnabled(true);
				}
			}
		} // close keyReleased

		public void keyTyped(KeyEvent arg0) {
			if (chatEntry.getText().equals("")) {
				send.setEnabled(false);
			} else {
				send.setEnabled(true);
			}
		} // close keyTyped
		
	} // close EnterKeyListener
	
	class ConnectListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			if (connect.getText().equals("Connect")) {
				Connect.Reconnect reconnect = user.new Reconnect();
				Thread reconnectThread = new Thread(reconnect);
				reconnectThread.setName("Reconnect (from Connect click)");
				reconnectThread.start();
			} else {
				user.disconnect();
				updateTextArea("You have successfully disconnected from the server. You may reconnect at any time by pressing the Connect button.");
			}
		} // close actionPerformed
		
	} // close ConnectListener
	
	// puts username in title
	public void setTitle(String username) {
		final String USERNAME = username;
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				frame.setTitle("Battleship - " + USERNAME);
			}
		});
		
	} // close setTitle
	
	// sets up input dialog for retrieving a username
	public String promptUsername() {
		String username = JOptionPane.showInputDialog("Select a username:");
		return username;
	} // close promptUsername
	
	// sets up input dialog for retrieving the server's IP
	public String getServerIP() {
		String ip = JOptionPane.showInputDialog("Enter the IP address of the Battleship server (<press enter> = localhost):");
		return ip;
	} // close getServerIP
	
	// sets up input dialog for retrieving the server's port
	public int getServerPort() {
		boolean invalidPort = true;
		int port = 2014;
		while (invalidPort) {
			String rawPort = JOptionPane.showInputDialog("Enter the port number (<press enter> = 2014):");
			try {
				if (rawPort.equals("")) {
					invalidPort = false;
				} else {
					port = Integer.parseInt(rawPort);
					if (port < 1024 || port > 65535) {
						JOptionPane.showConfirmDialog(frame, "Invalid port Number.", "Error", JOptionPane.CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
					} else {
						invalidPort = false;
					}
				}
			} catch (NumberFormatException e) {
				JOptionPane.showConfirmDialog(frame, "Invalid port Number.", "Error", JOptionPane.CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
			}
		}
		return port;
	} // close getServerPort
	
	// dialog indicating a bad username has been selected
	public void showBadUsername(String usernames) {
		if (!usernames.equals("")) {
			JOptionPane.showMessageDialog(frame, "Do not enter a username with fewer than 2 characters, greater than 15 characters, or one that matches another user's.\nThese users are currently logged in: " + usernames, "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(frame, "Do not enter a username with fewer than 2 characters, greater than 15 characters, or one that matches another user's.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	} // close showBadUsername
	
	public void showNullUsername() {
		JOptionPane.showMessageDialog(frame, "You must enter a username.", "Error", JOptionPane.ERROR_MESSAGE);
	} // close showNullUsername
	
	// resets fields back to starting position after a game has ended or a client has disconnected
	public void resetFields() {
		java.awt.EventQueue.invokeLater(new Runnable() {
		      public void run() {
		    	for (int i = 0; i < 49; i++) {
		  			userField[i].setText("");
		  			userField[i].setBackground(UNCLICKED_NOT_TURN);
		  			userClicked[i] = false;
		  			enemyField[i].setEnabled(true);
		  			enemyField[i].setText("");
		  			enemyField[i].setBackground(UNCLICKED_NOT_TURN);
		  			enemyClicked[i] = false;
		  		}
		  		chatEntry.setText("");
		      }
		});
		
	} // close resetFields
	
	// allows the chat box to be typed into
	public void enableChat(boolean enable) {
		final boolean ENABLE = enable;
		java.awt.EventQueue.invokeLater(new Runnable() {
		      public void run() {
		    	  chatEntry.setEnabled(ENABLE);
		      }
		});
		
	} // close enableChat
	
	public void setConnected(boolean connected) {
		final boolean CONNECTED = connected;
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
		    	if (CONNECTED) {
		  			connect.setText("Disconnect");
		  		} else {
		  			resetFields();
		  			enableChat(false);
		  			connect.setText("Connect");
		  		}
			}
		});
	} // close setConnected
	
} // close UserFrontEnd